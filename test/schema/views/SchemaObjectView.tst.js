import SchemaObjectView from '../../../src/schema/views/SchemaObjectView';
import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import {TestSchema, TestModel} from '../models/TestModel';

describe('SchemaObjectView tests', () => {
    it('given nothing, SchemaObjectView should complain to console about missing props', () => {
        //setup
        const consoleErrorSpy = jest.spyOn(console, 'error');

        //when
        renderer.create(<SchemaObjectView/>);

        //then
        expect(consoleErrorSpy).toHaveBeenCalledTimes(6);

        //cleanup
        consoleErrorSpy.mockRestore();
    });

    it('given everything, SchemaObjectView should not complain to console about missing props', () => {
        //setup
        const consoleErrorSpy = jest.spyOn(console, 'error');

        //when
        renderer.create(<SchemaObjectView schema={TestSchema} model={TestModel} onClear={jest.fn()} onSave={jest.fn()} onDelete={jest.fn()} instance={new TestModel()}/>);

        //then
        expect(consoleErrorSpy).not.toHaveBeenCalled();

        //cleanup
        consoleErrorSpy.mockRestore();
    });

    it('onChange does stuff to instance', () => {
        //setup
        Enzyme.configure({adapter: new Adapter()});
        let instance = new TestModel();
        const component = shallow(<SchemaObjectView schema={TestSchema} model={TestModel} onClear={jest.fn()} onSave={jest.fn()} onDelete={jest.fn()} instance={instance}/>).instance();
        const setStateSpy = jest.spyOn(component, 'setState');

        //when
        component.onChange('name', 'Your Mom');

        //then
        expect(setStateSpy).toHaveBeenCalledTimes(1);
        expect(instance.name).toBe('Your Mom');

        //cleanup
        setStateSpy.mockRestore();
    });
});