import SchemaObjectManagementView from '../../../src/schema/views/SchemaObjectManagementView';
import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import {TestModel, TestSchema} from '../models/TestModel';

describe('SchemaObjectManagementView tests', () => {
    let component;

    // beforeEach(() => {
    //     Enzyme.configure({adapter: new Adapter()});
    //     component = shallow(<SchemaObjectManagementView schema={TestSchema} model={TestModel} modelName='Test' />).instance();
    // });
    //
    // afterEach(() => {
    //     TestModel.deleteMany({});
    // });

    it('findOptions, happy path', async () => {
        //setup
        const modelFindSpy = jest.spyOn(TestModel, 'find');
        const setStateSpy = jest.spyOn(component, 'setState');

        //when: no query passed
        await component.findOptions();

        //then
        expect(modelFindSpy).toHaveBeenCalledTimes(1);
        expect(modelFindSpy).toHaveBeenCalledWith({}, expect.anything());
        //TODO: this isn't working and it doesn't appear that the callback send to our spy is even called?
        //async and await don't seem to be working? or it might be the spying
        //maybe set up data in the db and make this an integration test?
        // expect(setStateSpy).toHaveBeenCalledTimes(1);

        //cleanup
        modelFindSpy.mockRestore();
        setStateSpy.mockRestore();
    });

    it('findOptions, unhappy path', async () => {
        //setup
        // Enzyme.configure({adapter: new Adapter()});
        // const component = shallow(<SchemaObjectManagementView/>).instance();
        // const modelFindSpy = jest.spyOn(TestModel, 'find');
        // const setStateSpy = jest.spyOn(component, 'setState');
        //
        // //when: no query passed
        // await component.findPersons();
        //
        // //then
        // expect(modelFindSpy).toHaveBeenCalledTimes(1);
        // expect(modelFindSpy).toHaveBeenCalledWith({}, expect.anything());
        // //TODO: this isn't working and it doesn't appear that the callback send to our spy is even called?
        // // expect(setStateSpy).toHaveBeenCalledTimes(1);
        //
        // //cleanup
        // modelFindSpy.mockRestore();
        // setStateSpy.mockRestore();
    });

    // it('onChangeSelected', async () => {
    //     //setup
    //     component.state.options = [{id: 'some id', name: 'found me!'}, {id: 'other id', name: "can't find me!"}];
    //
    //     //when
    //     await component.onChangeSelected('some id');
    //
    //     //then
    //     expect(component.state.selected.name).toBe('found me!');
    // });
    //
    // describe('onSaveSelected', () => {
    //     const testCases = [
    //         {name: 'happy path', callbackParam: '', saveCalls: 1, findCalls: 1, errorCalls: 0},
    //         {name: 'unhappy path', callbackParam: 'some error', saveCalls: 1, findCalls: 0, errorCalls: 1}
    //     ];
    //
    //     testCases.forEach(testCase => {
    //         it(testCase, () => {
    //             //setup:
    //             let mockSave = jest.fn(callback => {
    //                 callback(testCase.callbackParam);
    //             });
    //             component.state.selected = {save: mockSave};
    //             let findOptionsSpy = jest.spyOn(component, 'findOptions');
    //             let consoleErrorSpy = jest.spyOn(console, 'error');
    //
    //             //when:
    //             component.onSaveSelected();
    //
    //             //then:
    //             expect(mockSave).toHaveBeenCalledTimes(testCase.saveCalls);
    //             expect(findOptionsSpy).toHaveBeenCalledTimes(testCase.findCalls);
    //             expect(consoleErrorSpy).toHaveBeenCalledTimes(testCase.errorCalls);
    //
    //             //cleanup:
    //             findOptionsSpy.mockRestore();
    //             consoleErrorSpy.mockRestore();
    //         });
    //     });
    // });
    //
    // it('onClearSelected', async () => {
    //     //setup:
    //     component.state.instance = {id: 'I am an instance'};
    //
    //     //when:
    //     await component.onClearSelected();
    //
    //     //then:
    //     expect(component.id).not.toBe('I am an instance');
    // });
    //
    // it('onDeleteSelected', async () => {
    //     //setup:
    //     const saved = new TestModel();
    //     saved.save();
    //     component.state.selected = saved;
    //     let spyDelete = jest.spyOn(TestModel, 'deleteOne');
    //     let consoleErrorSpy = jest.spyOn(console, 'error');
    //
    //     //when:
    //     await component.onDeleteSelected();
    //
    //     //then:
    //     expect(component.state.selected.name).not.toBeDefined();
    //     expect(component.state.options).toHaveLength(0);
    //     expect(spyDelete).toHaveBeenCalledTimes(1);
    //     expect(spyDelete).toHaveBeenCalledWith({_id: saved._id}, expect.anything());
    //     expect(consoleErrorSpy).toHaveBeenCalledTimes(0);
    //
    //     //cleanup:
    //     spyDelete.mockRestore();
    // });
});