import SelectObjectFromListView from '../../../src/schema/views/SelectObjectFromListView';
import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

describe('SelectObjectFromListView tests', () => {
    it('given nothing, SelectObjectFromListView should complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SelectObjectFromListView/>);

        //then
        expect(consoleErrorSpy).toHaveBeenCalledTimes(5);
        expect(consoleDebugSpy).toHaveBeenCalledTimes(1);

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('given everything, SelectObjectFromListView should not complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SelectObjectFromListView name={'name'} values={[]} selected={{}} idField={'id'} nameField={'name'} changeCallback={jest.fn()}/>);

        //then
        expect(consoleErrorSpy).not.toHaveBeenCalled();
        expect(consoleDebugSpy).not.toHaveBeenCalled();

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('onChange appropriately calls callback', () => {
        //setup:
        Enzyme.configure({adapter: new Adapter()});
        const mockCallback = jest.fn();
        const event = {
            target: {
                value: 'some value'
            }
        };
        const component = shallow(<SelectObjectFromListView name={'name'} values={[]} selected={{}} idField={'id'} nameField={'name'} changeCallback={mockCallback}/>).instance();

        //when
        component.onChange(event);

        //then
        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith('some value');
    });
});