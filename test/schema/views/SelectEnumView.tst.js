import SelectEnumView from '../../../src/schema/views/SelectEnumView';
import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

describe('SelectEnumView tests', () => {
    it('given nothing, SelectEnumView should complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SelectEnumView/>);

        //then
        expect(consoleErrorSpy).toHaveBeenCalledTimes(3);
        expect(consoleDebugSpy).toHaveBeenCalledTimes(1);

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('given everything, SelectEnumView should not complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SelectEnumView name={'name'} values={[]} selected={'something'} changeCallback={jest.fn()}/>);

        //then
        expect(consoleErrorSpy).not.toHaveBeenCalled();
        expect(consoleDebugSpy).not.toHaveBeenCalled();

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('onChange appropriately calls callback', () => {
        //setup:
        Enzyme.configure({adapter: new Adapter()});
        const mockCallback = jest.fn();
        const event = {
            target: {
                value: 'some value'
            }
        };
        const component = shallow(<SelectEnumView name={'name'} values={[]} selected={'something'} changeCallback={mockCallback}/>).instance();

        //when
        component.onChange(event);

        //then
        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith('name', 'some value');
    });
});