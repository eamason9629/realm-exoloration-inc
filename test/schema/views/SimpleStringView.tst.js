import SimpleStringView from '../../../src/schema/views/SimpleStringView';
import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

describe('SimpleStringView tests', () => {
    it('given nothing, the view should complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SimpleStringView/>);

        //then
        expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
        expect(consoleDebugSpy).toHaveBeenCalledTimes(1);

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('given everything, the view should not complain to console about missing props', () => {
        //setup:
        const consoleErrorSpy = jest.spyOn(console, 'error');
        const consoleDebugSpy = jest.spyOn(console, 'debug');

        //when
        renderer.create(<SimpleStringView name={'name'} value={'value'} changeCallback={jest.fn()}/>);

        //then
        expect(consoleErrorSpy).not.toHaveBeenCalled();
        expect(consoleDebugSpy).not.toHaveBeenCalled();

        //cleanup
        consoleErrorSpy.mockRestore();
        consoleDebugSpy.mockRestore();
    });

    it('onChange appropriately calls callback', () => {
        //setup:
        Enzyme.configure({adapter: new Adapter()});
        const mockCallback = jest.fn();
        const event = {
            target: {
                value: 'some value'
            }
        };
        const component = shallow(<SimpleStringView name={'name'} value={'value'} changeCallback={mockCallback}/>).instance();

        //when
        component.onChange(event);

        //then
        expect(mockCallback).toHaveBeenCalledTimes(1);
        expect(mockCallback).toHaveBeenCalledWith('name', 'some value');
    });
});