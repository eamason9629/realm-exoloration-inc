import Schema, {DEFAULT_STRING_SCHEMA, validateStringSchema, DEFAULT_NUMBER_SCHEMA, validateNumberSchema, DEFAULT_BOOLEAN_SCHEMA, validateBooleanSchema, DEFAULT_REFERENCE_SCHEMA, validateReferenceSchema, validateSchema, DEFAULT_DATE_SCHEMA, validateDateSchema, SCHEMA_ACTIONS, schemaReducer} from '../../../src/schema/schemas/Schema';
import TestSchema from './TestSchema';
import {GradeLevelValues} from '../../../src/person/enums/GradeLevel';
import moment from 'moment';

class NotTestSchema extends Schema {
    constructor() {
        super();
        this.id = 'an id'
    }

    prettyName() {
        return 'NotTestSchema';
    }

    defineSchema() {
        return {
            string: {
                type: String,
                initial: 'initial'
            },
            number: {
                type: Number,
                initial: 5
            }
        };
    }
}

it('TestSchema has a valid schema', () => {
    //setup:
    let testSchema = new TestSchema();

    //expect:
    expect(validateSchema(testSchema)).toEqual([]);
});

describe('Schema methods', () => {
    let schema;

    beforeEach(() => {
        schema = new Schema();
    });

    it('has an id', () => {
        //setup:
        let testSchema = new TestSchema();

        //expect:
        expect(schema.id).toBeDefined();
        expect(testSchema.id).toBeDefined();
    });

    it('fields are initialized', () => {
        //setup:
        let otherSchema = new NotTestSchema();

        //expect:
        expect(otherSchema).toHaveProperty('fields');
        expect(otherSchema).toHaveProperty('fields.string', 'initial');
        expect(otherSchema).toHaveProperty('fields.number', 5);
    });

    it('define schema is empty', () => {
        expect(schema.defineSchema()).toEqual({});
    });

    describe('get and set methods', () => {
        [
            {field: 'name', givenValue: 'your.mom'},
            {field: 'age', givenValue: 37},
            {field: 'isAwesome', givenValue: true},
        ].forEach(iter => {
            it(`non-Schema field ${iter.field} can be set and retrieved with value ${iter.givenValue}`, () => {
                //when: you set the value
                let result = schema.set(iter.field, iter.givenValue);

                //then: value set in fields and can be gotten
                expect(result).toBe(schema);
                expect(schema.fields).toHaveProperty(iter.field, iter.givenValue);
                expect(schema.get(iter.field)).toBe(iter.givenValue);
            });
        });

        [
            {field: 'simpleString', given: 'camelCase', expected: 'camelCase'},
            {field: 'complexString1', given: 'camelCase', expected: 'camelcase'},
            {field: 'complexString2', given: 'camelCase', expected: 'CAMELCASE'},
            {field: 'complexString2', given: ' camelCase ', expected: 'CAMELCASE'},
        ].forEach(iter => {
            it(`TestSchema field ${iter.field} is modified on set appropriately`, () => {
                //setup:
                let testSchema = new TestSchema();

                //when: you set the value
                let result = testSchema.set(iter.field, iter.given);

                //then: value set in fields and can be gotten
                expect(result).toBe(testSchema);
                expect(testSchema.fields).toHaveProperty(iter.field, iter.expected);
                expect(testSchema.get(iter.field)).toEqual(iter.expectedGet || iter.expected);
            });
        });

        let bdayMoment = moment('1981-02-01');
        let bdayDate = bdayMoment.toDate();

        [
            {given: bdayMoment, expected: bdayDate},
            {given: bdayDate, expected: bdayDate},
        ].forEach(iter => {
            it(`date fields are modified on set and get appropriately: isMoment: ${moment.isMoment(iter.given)}`, () => {
                //setup:
                let fieldName = 'dateField';
                let testSchema = new TestSchema();

                //when: you set the value
                let result = testSchema.set(fieldName, iter.given);

                //then: value set in fields and can be gotten
                expect(result).toBe(testSchema);
                expect(testSchema.fields).toHaveProperty(fieldName, iter.expected);
                expect(bdayMoment.isSame(testSchema.get(fieldName))).toBe(true);
            });
        });

        it('date fields are not created/returned when undefined', () => {
            //setup:
            let fieldName = 'dateField';
            let testSchema = new TestSchema();

            //when: you set the value
            let result = testSchema.set(fieldName, undefined);

            //then: value set in fields and can be gotten
            expect(result).toBe(testSchema);
            expect(testSchema.fields).toHaveProperty(fieldName, undefined);
            expect(testSchema.get(fieldName)).toBeUndefined();
        });
    });

    describe('getAll and setAll methods', () => {
        [
            {name: 'empty + empty == empty', fields: {}, given: {}, expected: {}},
            {name: 'empty + non-empty == non-empty', fields: {}, given: {name: 'your.mom'}, expected: {name: 'your.mom'}},
            {name: 'non-empty + empty == non-empty', fields: {name: 'my.mom'}, given: {}, expected: {name: 'my.mom'}},
            {name: 'overriding a value', fields: {name: 'my.mom', age: 37}, given: {name: 'your.mom'}, expected: {name: 'your.mom', age: 37}},
            {name: 'additive change', fields: {name: 'my.mom'}, given: {age: 37}, expected: {name: 'my.mom', age: 37}},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                schema.fields = iter.fields;

                //when:
                let result = schema.setAll(iter.given);

                //then:
                expect(result).toBe(schema);
                expect(schema.fields).toEqual(iter.expected);
                expect(schema.getAll()).toEqual(iter.expected);
            });
        });
    });

    describe('validate string', () => {
        let testSchema;

        beforeEach(() => {
            testSchema = new TestSchema();
        });

        [
            {name: 'valid', key: 'simpleString', data: {simpleString: 'simple'}, expected: []},
            {name: 'wrong type', key: 'simpleString', data: {simpleString: 5}, expected: ['field must be a String']},
            {name: 'complexString1: valid value', key: 'complexString1', data: {complexString1: ' valid'}, expected: []},
            {name: 'complexString1: not present', key: 'complexString1', data: {}, expected: ['field is required']},
            {name: 'complexString1: fails pattern', key: 'complexString1', data: {complexString1: 'invalid'}, expected: ['field does not match pattern "/\\s.*/"']},
            {name: 'complexString1: too short', key: 'complexString1', data: {complexString1: ' '}, expected: ['field has a min length of 2, given length of 1']},
            {name: 'complexString1: too long', key: 'complexString1', data: {complexString1: ' way too long, I think'}, expected: ['field has a max length of 10, given length of 22']},
            {name: 'enumField: valid value', key: 'enumField', data: {enumField: 'GL6'}, expected: []},
            {name: 'enumField: invalid value', key: 'enumField', data: {enumField: 'nope'}, expected: [`field must be one of these values: ${GradeLevelValues}`]},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                testSchema.setAll(iter.data);

                //when:
                let result = testSchema.validateString(iter.key);

                //then:
                expect(result).toEqual(iter.expected);
            });
        });
    });

    describe('validate number', () => {
        let testSchema;

        beforeEach(() => {
            testSchema = new TestSchema();
        });

        [
            {name: 'numberField: valid: no value', key: 'numberField', data: {requiredNumber: 5}, expected: []},
            {name: 'numberField: valid: proper value', key: 'numberField', data: {requiredNumber: 5, numberField: 3}, expected: []},
            {name: 'wrong type', key: 'numberField', data: {numberField: '5'}, expected: ['field must be a Number']},
            {name: 'numberField: too low', key: 'numberField', data: {requiredNumber: 5, numberField: -2}, expected: ['field has a min value of 0, given value of -2']},
            {name: 'numberField: too high', key: 'numberField', data: {requiredNumber: 5, numberField: 200}, expected: ['field has a max value of 5, given value of 200']},
            {name: 'numberField: too high', key: 'numberField', data: {requiredNumber: 5, numberField: 200}, expected: ['field has a max value of 5, given value of 200']},
            {name: 'requiredNumber: absent', key: 'requiredNumber', data: {}, expected: ['field is required']},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                testSchema.setAll(iter.data);

                //when:
                let result = testSchema.validateNumber(iter.key);

                //then:
                expect(result).toEqual(iter.expected);
            });
        });
    });

    describe('validate boolean', () => {
        let testSchema;

        beforeEach(() => {
            testSchema = new TestSchema();
        });

        [
            {name: 'booleanField: valid: is true', key: 'booleanField', data: {booleanField: true}, expected: []},
            {name: 'booleanField: valid: is false', key: 'booleanField', data: {booleanField: false}, expected: []},
            {name: 'wrong type', key: 'booleanField', data: {booleanField: '5'}, expected: ['field must be a Boolean']},
            {name: 'booleanField: invalid: not present', key: 'booleanField', data: {}, expected: ['field is required']},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                testSchema.setAll(iter.data);

                //when:
                let result = testSchema.validateBoolean(iter.key);

                //then:
                expect(result).toEqual(iter.expected);
            });
        });
    });

    describe('validate date', () => {
        let testSchema;

        beforeEach(() => {
            testSchema = new TestSchema();
        });

        [
            {name: 'dateField with valid value', schema: {dateField: DEFAULT_DATE_SCHEMA}, data: {dateField: moment('1981-02-01')}, expected: []},
            {name: 'undefined dateField is okay when not required', schema: {dateField: DEFAULT_DATE_SCHEMA}, data: {}, expected: []},
            {name: 'date before minDate', schema: {dateField: {type: Date, minDate: moment('1981-02-02').toDate()}}, data: {dateField: moment('1981-02-01')}, expected: ['given date cannot be before Mon Feb 02 1981 00:00:00 GMT-0600 (CST)']},
            {name: 'date after maxDate', schema: {dateField: {type: Date, maxDate: moment('1981-01-31').toDate()}}, data: {dateField: moment('1981-02-01')}, expected: ['given date cannot be after Sat Jan 31 1981 00:00:00 GMT-0600 (CST)']},
            {name: 'date required, not present', schema: {dateField: {type: Date, required: true}}, data: {someField: '1'}, expected: ['field is required']},
            {name: 'date required, is present', schema: {dateField: {type: Date, required: true}}, data: {dateField: new Date()}, expected: []},
            {name: 'dateField is not a date', schema: {dateField: {type: Date}}, data: {dateField: 5}, expected: ['field must be a Date']},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                jest.spyOn(testSchema, 'defineSchema').mockImplementation(() => {
                    return iter.schema;
                });
                testSchema.setAll(iter.data);

                //when:
                let result = testSchema.validateDate('dateField');

                //then:
                expect(result).toEqual(iter.expected);
            });
        });
    });

    describe('validate reference', () => {
        let testSchema;

        beforeEach(() => {
            testSchema = new TestSchema();
        });

        [
            {name: 'valid referenceField', key: 'referenceField', data: {referenceField: new TestSchema()}, expected: []},
            {name: 'valid requiredReferenceField', key: 'requiredReferenceField', data: {requiredReferenceField: new TestSchema()}, expected: []},
            {name: 'missing requiredReferenceField', key: 'requiredReferenceField', data: {}, expected: ['field is required']},
            {name: 'referenceField: wrong type', key: 'referenceField', data: {referenceField: '5'}, expected: ['field must be of type TestSchema, got String']},
            {name: 'referenceField: plain object', key: 'referenceField', data: {referenceField: {}}, expected: ['field must be of type TestSchema, got Object']},
            {name: 'referenceField: wrong type', key: 'referenceField', data: {referenceField: new NotTestSchema()}, expected: ['field must be of type TestSchema, got NotTestSchema']},
        ].forEach(iter => {
            it(iter.name, () => {
                //setup:
                testSchema.setAll(iter.data);

                //when:
                let result = testSchema.validateReference(iter.key);

                //then:
                expect(result).toEqual(iter.expected);
            });
        });
    });

    it('validate', () => {
        //setup:
        let testSchema = new TestSchema();
        let validateStringSpy = jest.spyOn(testSchema, 'validateString').mockImplementation(() => {
            return ['string error'];
        });
        let validateNumberSpy = jest.spyOn(testSchema, 'validateNumber').mockImplementation(() => {
            return ['number error'];
        });
        let validateBooleanSpy = jest.spyOn(testSchema, 'validateBoolean').mockImplementation(() => {
            return ['boolean error'];
        });
        let validateDateSpy = jest.spyOn(testSchema, 'validateDate').mockImplementation(() => {
            return ['date error'];
        });
        let validateReferenceSpy = jest.spyOn(testSchema, 'validateReference').mockImplementation(() => {
            return ['reference error'];
        });

        //when:
        let result = testSchema.validate();

        expect(result).toEqual([
            {field: 'simpleString', errors: ['string error']},
            {field: 'complexString1', errors: ['string error']},
            {field: 'complexString2', errors: ['string error']},
            {field: 'enumField', errors: ['string error']},
            {field: 'numberField', errors: ['number error']},
            {field: 'requiredNumber', errors: ['number error']},
            {field: 'referenceField', errors: ['reference error']},
            {field: 'requiredReferenceField', errors: ['reference error']},
            {field: 'dateField', errors: ['date error']},
        ]);
    });
});

it('String schema is what we expect', () => {
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('type', String);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('required', false);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('pattern', /.*/);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('lowercase', false);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('uppercase', false);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('trim', false);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('minLength', 0);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('maxLength', Infinity);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('enum', []);
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('initial');
    expect(DEFAULT_STRING_SCHEMA).toHaveProperty('prettyName');
});

describe('String schema validator', () => {
    [
        {name: 'default is valid', schema: DEFAULT_STRING_SCHEMA, expected: []},
        {name: 'needs a type', schema: {}, expected: ['type must be String!']},
        {name: 'not of type String', schema: {type: Number}, expected: ['type must be String!']},
        {name: 'required is not a boolean', schema: {type: String, required: 5}, expected: ['required must be a boolean!']},
        {name: 'pattern is not valid', schema: {type: String, pattern: 'sel/\\'}, expected: ['the pattern "sel/\\" provided is not valid regex']},
        {name: 'lowercase is not a boolean', schema: {type: String, lowercase: 5}, expected: ['lowercase must be a boolean!']},
        {name: 'uppercase is not a boolean', schema: {type: String, uppercase: 5}, expected: ['uppercase must be a boolean!']},
        {name: 'trim is not a boolean', schema: {type: String, trim: 5}, expected: ['trim must be a boolean!']},
        {name: 'minLength is not a number', schema: {type: String, minLength: '5'}, expected: ['minLength must be a number!']},
        {name: 'maxLength is not a number', schema: {type: String, maxLength: '5'}, expected: ['maxLength must be a number!']},
        {name: 'enum is not an array', schema: {type: String, enum: '5'}, expected: ['enum must be an array!']},
        {name: 'two together', schema: {type: String, maxLength: '5', enum: 'your.mom'}, expected: ['maxLength must be a number!', 'enum must be an array!']},
        {name: 'uppercase and lowercase are mutually exclusive', schema: {type: String, lowercase: true, uppercase: true}, expected: ['uppercase and lowercase cannot both be true!']},
        {name: 'does not check both when one is not a boolean', schema: {type: String, lowercase: '5', uppercase: true}, expected: ['lowercase must be a boolean!']},
        {name: 'minLength cannot be more than maxLength', schema: {type: String, minLength: 5, maxLength: 1}, expected: ['minLength cannot be more than maxLength!']},
        {name: 'minLength can be the same as maxLength', schema: {type: String, minLength: 5, maxLength: 5}, expected: []},
        {name: 'minLength must be greater than or equal to 0', schema: {type: String, minLength: -5}, expected: ['minLength must be greater than or equal to 0!']},
        {name: 'maxLength must be greater than 0', schema: {type: String, maxLength: -5}, expected: ['maxLength must be greater than 0!']},
        {name: 'prettyName must be a String', schema: {type: String, prettyName: -5}, expected: ['prettyName must be a String!']},
        {name: 'prettyName can be a String', schema: {type: String, prettyName: "I'm so pretty!"}, expected: []},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateStringSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

it('Number schema is what we expect', () => {
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('type', Number);
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('required', false);
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('min', -Infinity);
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('max', Infinity);
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('initial');
    expect(DEFAULT_NUMBER_SCHEMA).toHaveProperty('prettyName');
});

describe('Number schema validator', () => {
    [
        {name: 'default is valid', schema: DEFAULT_NUMBER_SCHEMA, expected: []},
        {name: 'needs a type', schema: {}, expected: ['type must be Number!']},
        {name: 'required is not a boolean', schema: {type: Number, required: 5}, expected: ['required must be a boolean!']},
        {name: 'not of type Number', schema: {type: String}, expected: ['type must be Number!']},
        {name: 'min is not a number', schema: {type: Number, min: '5'}, expected: ['min must be a number!']},
        {name: 'max is not a number', schema: {type: Number, max: '5'}, expected: ['max must be a number!']},
        {name: 'two together', schema: {type: Number, min: '5', max: '5'}, expected: ['min must be a number!', 'max must be a number!']},
        {name: 'min cannot be more than max', schema: {type: Number, min: 5, max: 1}, expected: ['min cannot be more than max!']},
        {name: 'min can be the same as max', schema: {type: Number, minLminength: 5, max: 5}, expected: []},
        {name: 'prettyName must be a String', schema: {type: Number, prettyName: -5}, expected: ['prettyName must be a String!']},
        {name: 'prettyName can be a String', schema: {type: Number, prettyName: "I'm so pretty!"}, expected: []},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateNumberSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

it('Boolean schema is what we expect', () => {
    expect(DEFAULT_BOOLEAN_SCHEMA).toHaveProperty('type', Boolean);
    expect(DEFAULT_BOOLEAN_SCHEMA).toHaveProperty('initial');
    expect(DEFAULT_BOOLEAN_SCHEMA).toHaveProperty('prettyName');
});

describe('Boolean schema validator', () => {
    [
        {name: 'default is valid', schema: DEFAULT_BOOLEAN_SCHEMA, expected: []},
        {name: 'needs a type', schema: {}, expected: ['type must be Boolean!']},
        {name: 'not of type Boolean', schema: {type: String}, expected: ['type must be Boolean!']},
        {name: 'cannot have required', schema: {type: Boolean, required: 5}, expected: ['booleans cannot have a required!']},
        {name: 'initial must be a boolean: object', schema: {type: Boolean, initial: {}}, expected: ['initial value must be a boolean!']},
        {name: 'initial must be a boolean: undefined', schema: {type: Boolean, initial: undefined}, expected: ['initial value must be a boolean!']},
        {name: 'initial must be a boolean: string', schema: {type: Boolean, initial: 'true'}, expected: ['initial value must be a boolean!']},
        {name: 'initial must be a boolean: number', schema: {type: Boolean, initial: 1}, expected: ['initial value must be a boolean!']},
        {name: 'prettyName must be a String', schema: {type: Boolean, prettyName: -5}, expected: ['prettyName must be a String!']},
        {name: 'prettyName can be a String', schema: {type: Boolean, prettyName: "I'm so pretty!"}, expected: []},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateBooleanSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

it('Date schema is what we expect', () => {
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('type', Date);
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('required', false);
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('minDate', new Date(0));
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('maxDate', new Date(8640000000000000));
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('initial');
    expect(DEFAULT_DATE_SCHEMA).toHaveProperty('prettyName');
});

describe('Date schema validator', () => {
    [
        {name: 'default is valid', schema: DEFAULT_DATE_SCHEMA, expected: []},
        {name: 'needs a type', schema: {}, expected: ['type must be Date!']},
        {name: 'not of type Date', schema: {type: String}, expected: ['type must be Date!']},
        {name: 'required is not a boolean', schema: {type: Date, required: 5}, expected: ['required must be a boolean!']},
        {name: 'minDate must be a date: object', schema: {type: Date, minDate: {}}, expected: ['minDate value must be a date!']},
        {name: 'minDate must be a date: string', schema: {type: Date, minDate: 'true'}, expected: ['minDate value must be a date!']},
        {name: 'minDate must be a date: number', schema: {type: Date, minDate: 1}, expected: ['minDate value must be a date!']},
        {name: 'maxDate must be a date: object', schema: {type: Date, maxDate: {}}, expected: ['maxDate value must be a date!']},
        {name: 'maxDate must be a date: string', schema: {type: Date, maxDate: 'true'}, expected: ['maxDate value must be a date!']},
        {name: 'maxDate must be a date: number', schema: {type: Date, maxDate: 1}, expected: ['maxDate value must be a date!']},
        {name: 'initial must be a date: object', schema: {type: Date, initial: {}}, expected: ['initial value must be a date!']},
        {name: 'initial must be a date: string', schema: {type: Date, initial: 'true'}, expected: ['initial value must be a date!']},
        {name: 'initial must be a date: number', schema: {type: Date, initial: 1}, expected: ['initial value must be a date!']},
        {name: 'prettyName must be a String', schema: {type: Date, prettyName: -5}, expected: ['prettyName must be a String!']},
        {name: 'prettyName can be a String', schema: {type: Date, prettyName: "I'm so pretty!"}, expected: []},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateDateSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

it('Reference schema is what we expect', () => {
    expect(DEFAULT_REFERENCE_SCHEMA).toHaveProperty('type', Schema);
    expect(DEFAULT_REFERENCE_SCHEMA).toHaveProperty('required', false);
    expect(DEFAULT_REFERENCE_SCHEMA).toHaveProperty('prettyName');
});

describe('Reference schema validator', () => {
    [
        {name: 'default is valid', schema: DEFAULT_REFERENCE_SCHEMA, expected: []},
        {name: 'needs a type', schema: {}, expected: ['type must inherit from Schema!']},
        {name: 'not of type Schema', schema: {type: String}, expected: ['type must inherit from Schema!']},
        {name: 'required is not a boolean', schema: {type: Schema, required: 5}, expected: ['required must be a boolean!']},
        {name: 'cannot have initial state: empty object', schema: {type: Schema, initial: {}}, expected: ['references cannot have an initial state!']},
        {name: 'cannot have initial state: undefined', schema: {type: Schema, initial: undefined}, expected: ['references cannot have an initial state!']},
        {name: 'cannot have initial state: string', schema: {type: Schema, initial: 'your.mom'}, expected: ['references cannot have an initial state!']},
        {name: 'cannot have initial state: number', schema: {type: Schema, initial: 5}, expected: ['references cannot have an initial state!']},
        {name: 'cannot have initial state: boolean true', schema: {type: Schema, initial: true}, expected: ['references cannot have an initial state!']},
        {name: 'cannot have initial state: boolean false', schema: {type: Schema, initial: false}, expected: ['references cannot have an initial state!']},
        {name: 'prettyName must be a String', schema: {type: Schema, prettyName: -5}, expected: ['prettyName must be a String!']},
        {name: 'prettyName can be a String', schema: {type: Schema, prettyName: "I'm so pretty!"}, expected: []},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateReferenceSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

describe('validate schema', () => {
    [
        {name: 'valid schema', schema: {stringField: DEFAULT_STRING_SCHEMA, numberField: DEFAULT_NUMBER_SCHEMA, booleanField: DEFAULT_BOOLEAN_SCHEMA, referenceField: DEFAULT_REFERENCE_SCHEMA}, expected: []},
        {
            name: 'valid nested schema', schema: {
                defineSchema: () => {
                    return {stringField: DEFAULT_STRING_SCHEMA, numberField: DEFAULT_NUMBER_SCHEMA, booleanField: DEFAULT_BOOLEAN_SCHEMA, referenceField: DEFAULT_REFERENCE_SCHEMA}
                }
            }, expected: []
        },
        {name: 'invalid String schema', schema: {stringField: {type: String, lowercase: 5}, numberField: DEFAULT_NUMBER_SCHEMA, booleanField: DEFAULT_BOOLEAN_SCHEMA, referenceField: DEFAULT_REFERENCE_SCHEMA}, expected: [{field: 'stringField', errors: ['lowercase must be a boolean!']}]},
        {name: 'invalid Number schema', schema: {stringField: DEFAULT_STRING_SCHEMA, numberField: {type: Number, min: '5'}, booleanField: DEFAULT_BOOLEAN_SCHEMA, referenceField: DEFAULT_REFERENCE_SCHEMA}, expected: [{field: 'numberField', errors: ['min must be a number!']}]},
        {name: 'invalid Boolean schema', schema: {stringField: DEFAULT_STRING_SCHEMA, numberField: DEFAULT_NUMBER_SCHEMA, booleanField: {type: Boolean, initial: undefined}, referenceField: DEFAULT_REFERENCE_SCHEMA}, expected: [{field: 'booleanField', errors: ['initial value must be a boolean!']}]},
        {name: 'invalid Reference schema', schema: {stringField: DEFAULT_STRING_SCHEMA, numberField: DEFAULT_NUMBER_SCHEMA, booleanField: DEFAULT_BOOLEAN_SCHEMA, referenceField: {type: Schema, initial: {}}}, expected: [{field: 'referenceField', errors: ['references cannot have an initial state!']}]},
        {name: 'all together now!', schema: {stringField: {type: String, lowercase: 5}, numberField: {type: Number, min: '5'}, booleanField: {type: Boolean, initial: undefined}, referenceField: {type: Schema, initial: {}}}, expected: [{field: 'stringField', errors: ['lowercase must be a boolean!']}, {field: 'numberField', errors: ['min must be a number!']}, {field: 'booleanField', errors: ['initial value must be a boolean!']}, {field: 'referenceField', errors: ['references cannot have an initial state!']}]},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = validateSchema(iter.schema);

            //then:
            expect(result).toEqual(iter.expected);
        });
    });
});

it('schema actions are what we expect', () => {
    expect(SCHEMA_ACTIONS).toEqual({
        changeTab: 'schema.changeTab',
        changeSchema: 'schema.changeSchema',
        create: 'schema.create',
        update: 'schema.update',
        save: 'schema.save',
        delete: 'schema.delete',
        select: 'schema.select'
    });
});

describe('schema reducer', () => {
    //setup:
    const setFunction = function (key, value) {
        this[key] = value;
    };
    const notTestSchema = new NotTestSchema();
    notTestSchema.id = 'predictable';

    //where:
    [
        {name: 'empty + empty = empty', action: {}, expected: {}},
        {name: 'undefined action does nothing', action: {type: undefined, id: 1234, key: 'yo', value: 'value'}, expected: {}},
        {name: 'save action does nothing', action: {type: SCHEMA_ACTIONS.save, id: 1234, key: 'yo', value: 'value'}, expected: {}},
        {name: 'unknown action does nothing', action: {type: 'meh', id: 1234, key: 'yo', value: 'value'}, expected: {}},
        {name: 'changeTab', action: {type: SCHEMA_ACTIONS.changeTab, newTab: 'test', listTitle: 'test', schema: NotTestSchema}, expected: {selectedTab: 'test', listTitle: 'test', namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {}}},
        {name: 'changeTab - export', given: {selectedTab: 'test', listTitle: 'test', namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {}}, action: {type: SCHEMA_ACTIONS.changeTab, newTab: 'export'}, expected: {selectedTab: 'export', [NotTestSchema.name]: {}}},
        {name: 'changeSchema, no namespace yet', action: {type: SCHEMA_ACTIONS.changeSchema, schema: NotTestSchema}, expected: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {}}},
        {name: 'changeSchema, existing namespace', given: {[NotTestSchema.name]: {stuff: 'exists'}}, action: {type: SCHEMA_ACTIONS.changeSchema, schema: NotTestSchema}, expected: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {stuff: 'exists'}}},
        {name: 'create, no namespace', given: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {}}, action: {type: SCHEMA_ACTIONS.create}, expected: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {'an id': new NotTestSchema()}, selected: new NotTestSchema()}},
        {name: 'create, existing namespace', given: {namespace: NotTestSchema.name, schema: NotTestSchema}, action: {type: SCHEMA_ACTIONS.create}, expected: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {'an id': new NotTestSchema()}, selected: new NotTestSchema()}},
        {name: 'update', given: {selected: {set: setFunction}}, action: {type: SCHEMA_ACTIONS.update, key: 'someKey', value: 'some value'}, expected: {selected: {set: setFunction, someKey: 'some value'}}},
        {name: 'delete', given: {selected: notTestSchema, namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {[notTestSchema.id]: notTestSchema}}, action: {type: SCHEMA_ACTIONS.delete}, expected: {namespace: NotTestSchema.name, schema: NotTestSchema, [NotTestSchema.name]: {}}},
        {name: 'select', given: {selected: new NotTestSchema(), namespace: NotTestSchema.name, [NotTestSchema.name]: {[notTestSchema.id]: notTestSchema}}, action: {type: SCHEMA_ACTIONS.select, id: notTestSchema.id}, expected: {selected: notTestSchema, namespace: NotTestSchema.name, [NotTestSchema.name]: {[notTestSchema.id]: notTestSchema}}},
    ].forEach(iter => {
        it(iter.name, () => {
            //when:
            let result = schemaReducer(iter.given || {}, iter.action);

            //then:
            expect(result).toEqual(iter.expected);
            expect(result).not.toBe(iter.given);
        });
    });
});
