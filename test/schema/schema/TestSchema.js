import Schema from '../../../src/schema/schemas/Schema';
import {GradeLevelValues} from '../../../src/person/enums/GradeLevel';

class TestSchema extends Schema {
    constructor() {
        super();
    }

    defineSchema() {
        return {
            simpleString: {
                type: String
            },
            complexString1: {
                type: String,
                required: true,
                pattern: /\s.*/,
                lowercase: true,
                minLength: 2,
                maxLength: 10
            },
            complexString2: {
                type: String,
                uppercase: true,
                trim: true
            },
            enumField: {
                type: String,
                enum: GradeLevelValues
            },
            numberField: {
                type: Number,
                min: 0,
                max: 5
            },
            requiredNumber: {
                type: Number,
                required: true
            },
            referenceField: {
                type: TestSchema
            },
            requiredReferenceField: {
                type: TestSchema,
                required: true
            },
            dateField: {
                type: Date
            }
        }
    }
}

export default TestSchema;
