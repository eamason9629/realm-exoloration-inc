import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import ApplicationView from './views/ApplicationView';
import {schemaReducer, SCHEMA_ACTIONS} from '../schema/schemas/Schema';
import PersonSchema from '../person/schemas/PersonSchema';
import SchemaObjectManagementView from '../schema/views/SchemaObjectManagementView';

const TABS = [
    {name: 'person', text: 'Person Management', listTitle: 'Existing People', schema: PersonSchema, component: <SchemaObjectManagementView key={'person'} schema={PersonSchema}/>},
];

const store = createStore(schemaReducer, {});
store.dispatch({type: SCHEMA_ACTIONS.changeTab, newTab: TABS[0].name, schema: TABS[0].schema, listTitle: TABS[0].listTitle});

ReactDOM.render(
    <Provider store={store}>
        <ApplicationView tabs={TABS}/>
    </Provider>,
    document.getElementById('root')
);
