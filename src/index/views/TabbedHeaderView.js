import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../../schema/schemas/Schema';

export class TabbedHeaderView extends Component {
    constructor() {
        super();
        this.openTab = this.openTab.bind(this);
    }

    openTab(event) {
        let tabName = event.target.dataset.name;
        let tab = this.props.tabs.find(tab => {
            return tab.name === tabName;
        });
        this.props.dispatch({type: SCHEMA_ACTIONS.changeTab, newTab: tab.name, schema: tab.schema, listTitle: tab.listTitle});
    }

    render() {
        return (
            <header>
                {this.props.tabs.map(tab => {
                    return <a key={tab.name} className={(tab.name === this.props.selectedTab) ? 'selected-tab' : 'not-selected-tab'} href='#' title={tab.text} data-name={tab.name} onClick={this.openTab}>{tab.text}</a>;
                })}
            </header>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedTab: state.selectedTab
    };
};

export default connect(
    mapStateToProps,
    null,
)(TabbedHeaderView);