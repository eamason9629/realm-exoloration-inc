import React, {Component} from 'react';
import TabbedHeaderView from './TabbedHeaderView';
import {connect} from 'react-redux';

export class ApplicationView extends Component {
    render() {
        return (
            <div>
                <TabbedHeaderView tabs={this.props.tabs}/>
                {this.props.tabs.find(tab => {
                    return tab.name === this.props.selectedTab;
                }).component}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedTab: state.selectedTab
    };
};

export default connect(
    mapStateToProps,
    null,
)(ApplicationView);