import React, {Component} from 'react';
import SelectEnumView from './SelectEnumView';
import SimpleStringView from './SimpleStringView';
import DateFieldView from './DateFieldView';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../schemas/Schema';
import ReferenceFieldView from './ReferenceFieldView';

class SchemaObjectView extends Component {
    constructor() {
        super();

        this.render = this.render.bind(this);
        this.calculateFields = this.calculateFields.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onNew = this.onNew.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    onSave() {
        this.props.dispatch({type: SCHEMA_ACTIONS.save});
    }

    onNew() {
        this.props.dispatch({type: SCHEMA_ACTIONS.create});
    }

    onDelete() {
        this.props.dispatch({type: SCHEMA_ACTIONS.delete});
    }

    calculateFields() {
        let fields = [];
        let schema = this.props.selected.defineSchema();
        Object.keys(schema).forEach(key => {
            let field = schema[key];
            field.name = key;
            fields.push(field);
        });
        return fields;
    }

    render() {
        if (this.props.selected) {
            let fields = this.calculateFields();

            return (
                <div>
                    {fields.map(field => {
                        switch (field.type.name) {
                            case 'Date':
                                return <DateFieldView key={field.name} name={field.name} prettyName={field.prettyName || field.name}/>;
                            case 'Boolean': //TODO
                            case 'Number':  //TODO
                            case 'String':
                                if (field.enum) {
                                    return <SelectEnumView key={field.name} name={field.name} prettyName={field.prettyName || field.name} values={field.enum}/>;
                                } else {
                                    return <SimpleStringView key={field.name} name={field.name} prettyName={field.prettyName || field.name}/>;
                                }
                            default:
                                return <ReferenceFieldView key={field.name} name={field.name} prettyName={field.prettyName || field.name} namespace={field.type.name}/>;
                        }
                    })}
                    <div className='button-bar'>
                        <button onClick={this.onSave}>Save</button>
                        <button onClick={this.onNew}>New</button>
                        <button onClick={this.onDelete}>Delete</button>
                    </div>
                </div>
            )
        } else {
            return <div className='button-bar'><button onClick={this.onNew}>New</button></div>;
        }
    }
}

const mapStateToProps = (state) => {
    return {
        selected: state.selected,
    };
};

export default connect(
    mapStateToProps,
    null,
)(SchemaObjectView);