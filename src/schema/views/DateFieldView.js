import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../schemas/Schema';

export class DateFieldView extends Component {
    constructor(props) {
        super();

        this.render = this.render.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(value) {
        this.props.dispatch({type: SCHEMA_ACTIONS.update, key: this.props.name, value: value.toDate()});
    }

    render() {
        return (
            <div key={this.props.name} className='date-field form-field'>
                <label htmlFor={this.props.name}>{this.props.prettyName}</label>
                <DatePicker selected={this.props.selected.get(this.props.name)} onChange={this.onChange}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selected: state.selected,
    };
};

export default connect(
    mapStateToProps,
    null,
)(DateFieldView);