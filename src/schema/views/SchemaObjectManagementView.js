import React, {Component} from 'react';
import SchemaObjectView from '../../schema/views/SchemaObjectView';
import SelectObjectFromListView from '../../schema/views/SelectObjectFromListView';
import {connect} from 'react-redux';

export class SchemaObjectManagementView extends Component {
    render() {
        return (
            <div>
                <SelectObjectFromListView idField='id' nameField='name'/>
                <hr/>
                <SchemaObjectView key={this.props.selected ? this.props.selected.id : 'new'}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selected: state.selected,
    };
};

export default connect(
    mapStateToProps,
    null,
)(SchemaObjectManagementView);