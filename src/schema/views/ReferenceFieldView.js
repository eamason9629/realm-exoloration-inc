import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../schemas/Schema';

export class ReferenceFieldView extends Component {
    constructor() {
        super();

        this.onChange = this.onChange.bind(this);
    }

    calculateOptions() {
        return Object.values(this.props.state[this.props.namespace]) || [];
    }

    onChange(event) {
        let value = this.calculateOptions().find(option => {
            return option.id === event.target.value;
        });
        this.props.dispatch({type: SCHEMA_ACTIONS.update, key: this.props.name, value: value});
    }

    render() {
        let value = this.props.selected.get(this.props.name);
        let options = this.calculateOptions();

        return (
            <div className='select-field form-field'>
                <label htmlFor={this.props.name}>{this.props.prettyName}</label>
                <select name={this.props.name} value={value ? value.id : 'empty'} onChange={this.onChange}>
                    <option disabled value='empty'>Please select an option...</option>
                    {options.map(option => {
                        return <option key={option.id} value={option.id}>{option.get('name')}</option>
                    })}
                </select>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
        selected: state.selected,
    };
};

export default connect(
    mapStateToProps,
    null,
)(ReferenceFieldView);