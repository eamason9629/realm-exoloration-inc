import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../schemas/Schema';

export class SelectObjectFromListView extends Component {
    constructor() {
        super();

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.dispatch({type: SCHEMA_ACTIONS.select, id: event.target.value});
    }

    render() {
        return (
            <div className='select-field form-field'>
                <label htmlFor={this.props.schema.name}>{this.props.listTitle}</label>
                <select name={this.props.schema.name} value={this.props.selected ? this.props.selected[this.props.idField] : 'empty'} onChange={this.onChange}>
                    <option disabled value='empty'>Please select an option...</option>
                    {(this.props.options || []).map(value => {
                        return <option key={value.id} value={value.id}>{value.get(this.props.nameField)}</option>
                    })}
                </select>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        schema: state.schema,
        selected: state.selected,
        listTitle: state.listTitle,
        options: Object.values(state[state.namespace])
    };
};

export default connect(
    mapStateToProps,
    null,
)(SelectObjectFromListView);