import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SCHEMA_ACTIONS} from '../schemas/Schema';

export class SimpleStringView extends Component {
    constructor() {
        super();

        this.render = this.render.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.dispatch({type: SCHEMA_ACTIONS.update, key: this.props.name, value: event.target.value});
    }

    render() {
        return (
            <div key={this.props.name} className='simple-string-field form-field'>
                <label htmlFor={this.props.name}>{this.props.prettyName}</label>
                <input name={this.props.name} value={this.props.selected.get(this.props.name)} type='text' onChange={this.onChange}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selected: state.selected,
    };
};

export default connect(
    mapStateToProps,
    null,
)(SimpleStringView);