import moment from 'moment';

function addSchemas(first, second) {
    let result = {};
    Object.keys(first).forEach(key => {
        result[key] = first[key];
    });
    Object.keys(second).forEach(key => {
        result[key] = second[key];
    });
    return result;
}

function generateGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

class Schema {
    constructor() {
        this.id = generateGuid();
        this.fields = {};
        this.initializeFields();
    }

    defineSchema() {
        return {};
    }

    prettyName() {
        return 'Schema';
    }

    initializeFields() {
        let schema = this.defineSchema();
        Object.keys(schema).forEach(key => {
            let initialValue = schema[key].initial;
            if (initialValue) {
                this.set(key, initialValue);
            }
        });
    }

    set(name, value) {
        let theValue = value;
        let fieldSchema = this.defineSchema()[name] || {};
        let field = addSchemas(defaultStringSchema, fieldSchema);
        if (field.type === String) {
            if (field.uppercase) {
                theValue = theValue.toUpperCase();
            } else if (field.lowercase) {
                theValue = theValue.toLowerCase();
            }
            if (field.trim) {
                theValue = theValue.trim();
            }
        } else if (field.type === Date && typeof(theValue) !== 'undefined' && moment.isMoment(theValue)) {
            theValue = theValue.toDate();
        }
        this.fields[name] = theValue;
        return this;
    }

    setAll(values) {
        Object.keys(values).forEach(key => {
            this.set(key, values[key]);
        });
        return this;
    }

    get(name) {
        let fieldSchema = this.defineSchema()[name];
        let value = this.fields[name];
        if (fieldSchema && fieldSchema.type === Date && typeof(value) !== 'undefined') {
            return moment(value);
        }
        return value;
    }

    getAll() {
        return this.fields;
    }

    validateString(key) {
        let value = this.get(key);
        let field = addSchemas(defaultStringSchema, this.defineSchema()[key]);
        let problems = [];
        if (field.required && typeof(value) === 'undefined') {
            problems.push('field is required');
        }
        if (typeof(value) !== 'undefined') {
            if (typeof(value) !== 'string') {
                problems.push('field must be a String');
            }
            if (field.pattern && !new RegExp(field.pattern).exec(value)) {
                problems.push(`field does not match pattern "${field.pattern}"`);
            }
            if (typeof(field.minLength) !== 'undefined' && value.length < field.minLength) {
                problems.push(`field has a min length of ${field.minLength}, given length of ${value.length}`);
            }
            if (typeof(field.maxLength) !== 'undefined' && value.length > field.maxLength) {
                problems.push(`field has a max length of ${field.maxLength}, given length of ${value.length}`);
            }
            if (field.enum.length > 0 && field.enum.indexOf(value) === -1) {
                problems.push(`field must be one of these values: ${field.enum}`);
            }
        }
        return problems;
    }

    validateNumber(key) {
        let value = this.get(key);
        let field = addSchemas(defaultNumberSchema, this.defineSchema()[key]);
        let problems = [];
        if (typeof(value) !== 'undefined' && typeof(value) !== 'number') {
            problems.push('field must be a Number');
        }
        if (field.required && typeof(value) === 'undefined') {
            problems.push('field is required');
        }
        if (typeof(field.min) !== 'undefined' && value < field.min) {
            problems.push(`field has a min value of ${field.min}, given value of ${value}`);
        }
        if (typeof(field.max) !== 'undefined' && value > field.max) {
            problems.push(`field has a max value of ${field.max}, given value of ${value}`);
        }
        return problems;
    }

    validateBoolean(key) {
        let value = this.get(key);
        let problems = [];
        if (typeof(value) !== 'undefined' && typeof(value) !== 'boolean') {
            problems.push('field must be a Boolean');
        }
        if (typeof(value) === 'undefined') {
            problems.push('field is required');
        }
        return problems;
    }

    validateDate(key) {
        let value = this.get(key);
        let field = addSchemas(defaultDateSchema, this.defineSchema()[key]);
        let problems = [];
        if (typeof(this.fields[key]) !== 'undefined' && !(this.fields[key] instanceof Date)) {
            problems.push('field must be a Date');
        }
        if (field.required) {
            console.error('field value', value);
        }
        if (field.required && typeof(value) === 'undefined') {
            problems.push('field is required');
        }
        if (typeof(value) !== 'undefined' && moment(field.minDate).isAfter(value)) {
            problems.push(`given date cannot be before ${field.minDate}`);
        }
        if (typeof(value) !== 'undefined' && moment(field.maxDate).isBefore(value)) {
            problems.push(`given date cannot be after ${field.maxDate}`);
        }
        return problems;
    }

    validateReference(key) {
        let value = this.get(key);
        let field = addSchemas(defaultReferenceSchema, this.defineSchema()[key]);
        let problems = [];
        if (typeof(value) !== 'undefined' && !(value instanceof field.type)) {
            problems.push(`field must be of type ${field.type.name}, got ${value.constructor.name}`);
        }
        if (field.required && typeof(value) === 'undefined') {
            problems.push('field is required');
        }
        return problems;
    }

    validate() {
        let problems = [];
        let schema = this.defineSchema();
        Object.keys(schema).forEach(key => {
            let type = schema[key].type;
            let fieldErrors = [];
            switch (type) {
                case String:
                    fieldErrors = this.validateString(key);
                    break;
                case Number:
                    fieldErrors = this.validateNumber(key);
                    break;
                case Boolean:
                    fieldErrors = this.validateBoolean(key);
                    break;
                case Date:
                    fieldErrors = this.validateDate(key);
                    break;
                default:
                    fieldErrors = this.validateReference(key);
            }
            if (fieldErrors.length > 0) {
                problems.push({field: key, errors: fieldErrors});
            }
        });
        return problems;
    }
}

export default Schema;

const defaultStringSchema = {
    type: String,
    required: false,
    pattern: /.*/,
    lowercase: false,
    uppercase: false,
    trim: false,
    minLength: 0,
    maxLength: Infinity,
    enum: [],
    initial: undefined,
    prettyName: undefined
};

export {defaultStringSchema as DEFAULT_STRING_SCHEMA};

function stringSchemaValidator(toTest) {
    let problems = [];
    if (toTest.type !== String) {
        problems.push('type must be String!');
    }
    if (typeof(toTest.required) !== 'undefined' && typeof(toTest.required) !== 'boolean') {
        problems.push('required must be a boolean!');
    }
    try {
        new RegExp(toTest.pattern);
    } catch (e) {
        if (e.message.startsWith('Invalid regular expression:')) {
            problems.push(`the pattern "${toTest.pattern}" provided is not valid regex`);
        } else {
            throw e;
        }
    }
    if (typeof(toTest.lowercase) !== 'undefined' && typeof(toTest.lowercase) !== 'boolean') {
        problems.push('lowercase must be a boolean!');
    }
    if (typeof(toTest.uppercase) !== 'undefined' && typeof(toTest.uppercase) !== 'boolean') {
        problems.push('uppercase must be a boolean!');
    }
    if (typeof(toTest.trim) !== 'undefined' && typeof(toTest.trim) !== 'boolean') {
        problems.push('trim must be a boolean!');
    }
    if (typeof(toTest.minLength) !== 'undefined' && typeof(toTest.minLength) !== 'number') {
        problems.push('minLength must be a number!');
    }
    if (typeof(toTest.maxLength) !== 'undefined' && typeof(toTest.maxLength) !== 'number') {
        problems.push('maxLength must be a number!');
    }
    if (typeof(toTest.enum) !== 'undefined' && toTest.enum.constructor !== Array) {
        problems.push('enum must be an array!');
    }
    if (toTest.uppercase && typeof(toTest.uppercase) === 'boolean' && toTest.lowercase && typeof(toTest.lowercase) === 'boolean') {
        problems.push('uppercase and lowercase cannot both be true!');
    }
    if (typeof(toTest.minLength) !== 'undefined' && typeof(toTest.maxLength) !== 'undefined' && toTest.minLength > toTest.maxLength) {
        problems.push('minLength cannot be more than maxLength!');
    }
    if (toTest.minLength < 0) {
        problems.push('minLength must be greater than or equal to 0!');
    }
    if (toTest.maxLength <= 0) {
        problems.push('maxLength must be greater than 0!');
    }
    if (typeof(toTest.prettyName) !== 'undefined' && typeof(toTest.prettyName) !== 'string') {
        problems.push('prettyName must be a String!');
    }
    return problems;
}

export {stringSchemaValidator as validateStringSchema};

const defaultNumberSchema = {
    type: Number,
    required: false,
    min: -Infinity,
    max: Infinity,
    initial: undefined,
    prettyName: undefined
};

export {defaultNumberSchema as DEFAULT_NUMBER_SCHEMA};

function numberSchemaValidator(toTest) {
    let problems = [];
    if (toTest.type !== Number) {
        problems.push('type must be Number!');
    }
    if (typeof(toTest.required) !== 'undefined' && typeof(toTest.required) !== 'boolean') {
        problems.push('required must be a boolean!');
    }
    if (typeof(toTest.min) !== 'undefined' && typeof(toTest.min) !== 'number') {
        problems.push('min must be a number!');
    }
    if (typeof(toTest.max) !== 'undefined' && typeof(toTest.max) !== 'number') {
        problems.push('max must be a number!');
    }
    if (typeof(toTest.min) !== 'undefined' && typeof(toTest.max) !== 'undefined' && toTest.min > toTest.max) {
        problems.push('min cannot be more than max!');
    }
    if (typeof(toTest.prettyName) !== 'undefined' && typeof(toTest.prettyName) !== 'string') {
        problems.push('prettyName must be a String!');
    }
    return problems;
}

export {numberSchemaValidator as validateNumberSchema}

const defaultBooleanSchema = {
    type: Boolean,
    initial: false,
    prettyName: undefined
};

export {defaultBooleanSchema as DEFAULT_BOOLEAN_SCHEMA};

function booleanSchemaValidator(toTest) {
    let problems = [];
    if (toTest.type !== Boolean) {
        problems.push('type must be Boolean!');
    }
    if (toTest.hasOwnProperty('required')) {
        problems.push('booleans cannot have a required!');
    }
    if (toTest.hasOwnProperty('initial') && typeof(toTest.initial) !== 'boolean') {
        problems.push('initial value must be a boolean!');
    }
    if (typeof(toTest.prettyName) !== 'undefined' && typeof(toTest.prettyName) !== 'string') {
        problems.push('prettyName must be a String!');
    }
    return problems;
}

export {booleanSchemaValidator as validateBooleanSchema}

const defaultDateSchema = {
    type: Date,
    required: false,
    minDate: new Date(0),
    maxDate: new Date(8640000000000000),
    initial: undefined,
    prettyName: undefined
};

export {defaultDateSchema as DEFAULT_DATE_SCHEMA};

function dateSchemaValidator(toTest) {
    let problems = [];
    if (toTest.type !== Date) {
        problems.push('type must be Date!');
    }
    if (typeof(toTest.required) !== 'undefined' && typeof(toTest.required) !== 'boolean') {
        problems.push('required must be a boolean!');
    }
    if (typeof(toTest.minDate) !== 'undefined' && !(toTest.minDate instanceof Date)) {
        problems.push('minDate value must be a date!');
    }
    if (typeof(toTest.maxDate) !== 'undefined' && !(toTest.maxDate instanceof Date)) {
        problems.push('maxDate value must be a date!');
    }
    if (typeof(toTest.initial) !== 'undefined' && !(toTest.initial instanceof Date)) {
        problems.push('initial value must be a date!');
    }
    if (typeof(toTest.prettyName) !== 'undefined' && typeof(toTest.prettyName) !== 'string') {
        problems.push('prettyName must be a String!');
    }
    return problems;
}

export {dateSchemaValidator as validateDateSchema}

const defaultReferenceSchema = {
    type: Schema,
    required: false,
    prettyName: undefined
};

export {defaultReferenceSchema as DEFAULT_REFERENCE_SCHEMA};

function referenceSchemaValidator(toTest) {
    let problems = [];
    if (!Schema.isPrototypeOf(toTest.type) && toTest.type !== Schema) {
        problems.push('type must inherit from Schema!');
    }
    if (typeof(toTest.required) !== 'undefined' && typeof(toTest.required) !== 'boolean') {
        problems.push('required must be a boolean!');
    }
    if (toTest.hasOwnProperty('initial')) {
        problems.push('references cannot have an initial state!');
    }
    if (typeof(toTest.prettyName) !== 'undefined' && typeof(toTest.prettyName) !== 'string') {
        problems.push('prettyName must be a String!');
    }
    return problems;
}

export {referenceSchemaValidator as validateReferenceSchema}

function schemaValidator(toTest) {
    let schema = toTest.defineSchema ? toTest.defineSchema() : toTest;
    let problems = [];
    Object.keys(schema).forEach(key => {
        let field = schema[key];
        let fieldProblems = [];
        switch (field.type) {
            case String:
                fieldProblems = stringSchemaValidator(field);
                break;
            case Number:
                fieldProblems = numberSchemaValidator(field);
                break;
            case Boolean:
                fieldProblems = booleanSchemaValidator(field);
                break;
            case Date:
                fieldProblems = dateSchemaValidator(field);
                break;
            default:
                fieldProblems = referenceSchemaValidator(field);
        }
        if (fieldProblems.length > 0) {
            problems.push({field: key, errors: fieldProblems});
        }
    });
    return problems;
}

export {schemaValidator as validateSchema}

const schemaActions = {
    changeTab: 'schema.changeTab',
    changeSchema: 'schema.changeSchema',
    create: 'schema.create',
    update: 'schema.update',
    save: 'schema.save',
    delete: 'schema.delete',
    select: 'schema.select'
};

export {schemaActions as SCHEMA_ACTIONS}

function schemaReducerFunction(state = {}, action) {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case schemaActions.changeTab:
            newState.selectedTab = action.newTab;
            if (newState.selectedTab === 'export') {
                delete newState.listTitle;
                delete newState.schema;
                delete newState.namespace;
            } else {
                newState.listTitle = action.listTitle;
                newState = schemaReducerFunction(newState, {type: schemaActions.changeSchema, schema: action.schema});
            }
            break;
        case schemaActions.changeSchema:
            newState.schema = action.schema;
            newState.namespace = newState.schema.name;
            if (!newState[newState.namespace]) {
                newState[newState.namespace] = {};
            }
            delete newState.selected;
            break;
        case schemaActions.create:
            newState.selected = new newState.schema();
            if (!newState[newState.namespace]) {
                newState[newState.namespace] = {};
            }
            newState[newState.namespace][newState.selected.id] = newState.selected;
            break;
        case schemaActions.update:
            newState.selected.set(action.key, action.value);
            break;
        case schemaActions.delete:
            let id = newState.selected.id;
            delete newState[newState.namespace][id];
            delete newState.selected;
            break;
        case schemaActions.select:
            newState.selected = newState[newState.namespace][action.id];
            break;
        case schemaActions.save:
        default:
            //no op right now
            console.debug(`unsupported action: ${action.type}`);
    }
    return newState;
}

export {schemaReducerFunction as schemaReducer};