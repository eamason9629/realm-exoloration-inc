import Schema from '../../schema/schemas/Schema';

class PersonSchema extends Schema {
    defineSchema() {
        return {
            name: {type: String},
            aID: {type: String},
            startDate: {type: Date},
            endDate: {type: Date},
            rate: {type: Number},
        }
    }
}

export default PersonSchema;
